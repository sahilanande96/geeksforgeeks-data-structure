"""
Given an array of size N. The task is to rotate array by D elements where D ≤ N.

Example 1:

Input:
N = 7
Arr[] = {1, 2, 3, 4, 5, 6, 7}
D = 2
Output: 3 4 5 6 7 1 2
Explanation: 
Rotate by 1: [2, 3, 4, 5, 6, 7, 1]
Rotate by 2: [3, 4, 5, 6, 7, 1, 2]

Example 2:

Input:
N = 4
Arr[] = {1, 3, 4, 2}
D = 3
Output: 2 4 3 1

Your Task:
You don't need to read input or print anything. Your task is to complete the function leftRotate() which takes the array of integers arr[], its size n and d as input parameters and rotates arr[] in-place without using any extra memory.


Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)


Constraints:
1 ≤ N ≤ 105
1 ≤ Arr[i] ≤ 1000
0 ≤ D ≤ N

"""

#Code
#User function Template for python3

class Solution:
    def leftRotate(self, arr, n, d):
        # code here

        # This also works, but later solution is faster
        # for j in range(d):
        #     temp = arr[0]
        #     arr.pop(0)
        #     arr.append(temp)

        sublist = []
        for _ in range(d):
            item = arr.pop(0)
            sublist.append(item)
        arr.extend(sublist)

        # Mention solution is,
        # rotate(arr[], d, n)
        # reverse(arr[], 1, d) ;
        # reverse(arr[], d + 1, n);
        # reverse(arr[], 1, n);


        # Reverse A to get ArB, where Ar is reverse of A.
        # Reverse B to get ArBr, where Br is reverse of B.
        # Reverse all to get (ArBr) r = BA.
        # Example : 
        # Let the array be arr[] = [1, 2, 3, 4, 5, 6, 7], d =2 and n = 7 
        # A = [1, 2] and B = [3, 4, 5, 6, 7] 
        

        # Reverse A, we get ArB = [2, 1, 3, 4, 5, 6, 7]
        # Reverse B, we get ArBr = [2, 1, 7, 6, 5, 4, 3]
        # Reverse all, we get (ArBr)r = [3, 4, 5, 6, 7, 1, 2]



#{ 
#  Driver Code Starts
#Initial Template for Python 3

if __name__ == '__main__':
    tc = int(input())
    while tc > 0:
        n = int(input())
        arr = list(map(int, input().strip().split()))
        d = int(input())
        ob = Solution()
        ob.leftRotate(arr, n, d)
        for xx in arr:
            print(xx, end=" ")
        print()
        tc -= 1

# } Driver Code Ends
